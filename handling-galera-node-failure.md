# Handling Galera Node Failure

Our Galera instances are provisioned using a StatefulSet. If a node with a galera instance fails we can take the following steps to recover:

## Recovery Steps

1. Delete the Persistant Volume Claim (PVC) for the bad Pod.
The Persistant Volume associated to the PVC has affinity to the node it started on, therefore until the node recovers the PV won't be available and the Pod will not recover automatically. To solve the problem of PV affinity, we need to delete the PVC of the failed Pod so the Pod can be rescheduled.
As long as other instances in the Galera cluster are working normally we can safely delete the PVC associated with the failed Pod. Once the recovery process has been completed, [Galera will automatically synchronise the data to the recovered instance](https://galeracluster.com/library/documentation/recovery.html).
2. Delete the failed Pod
Deleting the failed Pod will now force Kubernetes to reschedule the Pod on an available node.

## Scenario

We have 3 instances of a Galera running in a cluster.

```
# kubectl --namespace mariadb-galera get pods

mariadb-galera         mariadb-galera-0                           1/1     Running            0          16h          node1
mariadb-galera         mariadb-galera-1                           1/1     Running            0          16h          node2
mariadb-galera         mariadb-galera-2                           1/1     Running            0          16h          node3
```

Now imagine the real possabilty that node1 goes offline.

```
# kubectl get nodes

node1   Unknown                    worker   7d21h   v1.18.4+k3s1
node2   Ready                      worker   7d21h   v1.18.4+k3s1
node3   Ready                      worker   7d21h   v1.18.4+k3s1
node4   Ready                      worker   7d21h   v1.18.4+k3s1
```

Our Galera instance `mariadb-galera-0` will now be unavailable, stuck in a Terminating state. In our case we the Pod's events were reporting `0/5 nodes are available: 2 node(s) were unschedulable, 3 node(s) had volume node affinity conflict.`

```
# kubectl --namespace mariadb-galera get pods

mariadb-galera         mariadb-galera-1                           1/1     Running                0          16h          node2
mariadb-galera         mariadb-galera-2                           1/1     Running                0          16h          node3
mariadb-galera         mariadb-galera-0                           0/1     Terminating            0          16h          node1
```

We will now follow the Recovery Steps to get `mariadb-galera-0` back online safely.

```
kubectl --namespace mariadb-galera delete pvc data-mariadb-galera-0
```

The Pod events will now be reporting `persistentvolumeclaim "data-mariadb-galera-0" not found`. We can now delete the Pod.

```
kubectl --namespace mariadb-galera delete pod mariadb-galera-0
```

The Pod `mariadb-galera-0` will now be provisioned on to our available `node4` and be brought back in to the Galera cluster by the other 2 instances.

```
mariadb-galera         mariadb-galera-1                           1/1     Running            0          16h          node2
mariadb-galera         mariadb-galera-2                           1/1     Running            0          16h          node3
mariadb-galera         mariadb-galera-0                           1/1     Running            0          2m3s         node4
```